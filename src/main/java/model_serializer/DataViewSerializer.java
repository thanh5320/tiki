package main.java.model_serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.model.DataView;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class DataViewSerializer implements Serializer<DataView> {

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public byte[] serialize(String topic, DataView data) {
        byte[] retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            retVal = objectMapper.writeValueAsString(data).getBytes();
        } catch (Exception exception) {
            System.out.println("Error in serializing object"+ data.toString());
        }
        return retVal;
    }

    @Override
    public void close() {

    }
}
