package main.java.hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.spark.sql.ForeachWriter;
import org.apache.spark.sql.Row;

import java.io.IOException;

public class HbaseSink extends ForeachWriter<Row> {
    private Connection connection = null;

    @Override
    public boolean open(long partitionId, long version) {
        try{
            Configuration conf = HBaseConfiguration.create();
            conf.set("hbase.zookeeper.property.clientPort", "2181");
            conf.set("hbase.zookeeper.quorum","localhost");
            conf.set("hbase.master", "localhost:16000");
            connection = ConnectionFactory.createConnection(conf);
        }catch(IOException e){

        }
        return true;
    }

    @Override
    public void process(Row value) {
        String cpa = value.getAs("cpa_value").toString();
        String rowkey = value.getAs("guid").toString();
        String columnFamily = "cpa_value";
        Put put = new Put(rowkey.getBytes());
        put.addColumn(columnFamily.getBytes(), ("cpa1").getBytes(), cpa.getBytes());
        try{
            TableName tableName = TableName.valueOf ( "tiki");
            Table table = connection.getTable(tableName);
            table.put(put);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void close(Throwable errorOrNull) {
        try{
            connection.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
