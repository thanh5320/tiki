package main.java;

import main.java.spark.SparkJob;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.streaming.StreamingQueryException;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Main {
    public static void main(String[] args) throws TimeoutException, StreamingQueryException, IOException {
        SparkSession spark = SparkSession.builder().appName("task-tiki").getOrCreate();

       SparkJob sparkJob = new SparkJob(spark, "/home/thanhnv/task-tiki/data/2021_06_09", "/home/thanhnv/task-tiki/data/2021_06_09_view");
       //sparkJob.pushDataViewToKafka();
       //sparkJob.readDataViewFromKafka();
    }
}
