package main.java.model;

import java.io.Serializable;

public class DataView implements Serializable{
    private static final long serialVersionUID = 1L;
    private long timeCreate;
    private long guid;
    private String domain;
    private String path;
    /*
    private long cookieCreate;
    private int browserCode;
    private String browserVer;
    private int osCode;
    private String osVer;
    private long ip;


    private int geo;
    private int locId;
    private String flashver;
    private String jre;
    private int siteId;
    private int channelId;
    private String refer;
    private String sr;
    private String sc;
    private String fullRefer;
    private long requestid;
    private String fp_guid;
    private String category;
    private String googleId;
    private String tabActive;
     */

    public DataView(long timeCreate, long guid, String domain, String path) {
        this.timeCreate = timeCreate;
        this.guid = guid;
        this.domain = domain;
        this.path = path;
    }

    public long getTimeCreate() {
        return timeCreate;
    }

    public void setTimeCreate(long timeCreate) {
        this.timeCreate = timeCreate;
    }

    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
