package main.java.model;

import scala.Serializable;

public class DataEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    private long timeCreate;
    private String cpaValue;
    private long guid;
    /*
    private String clickId;
    private int isSuccess;
    private String domain;
    private String url;

    private String sourceType;
    private String cpaid;

     */
    private DataEvent(){}

    public DataEvent(long timeCreate, String cpaValue, long guid) {
        this.timeCreate = timeCreate;
        this.cpaValue = cpaValue;
        this.guid = guid;
    }

    public long getTimeCreate() {
        return timeCreate;
    }

    public void setTimeCreate(long timeCreate) {
        this.timeCreate = timeCreate;
    }

    public String getCpaValue() {
        return cpaValue;
    }

    public void setCpaValue(String cpaValue) {
        this.cpaValue = cpaValue;
    }

    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    @Override
    public String toString() {
        return timeCreate +"--" + cpaValue +"--"+guid;
    }
}
