package main.java.kafka;

public interface IKafkaConstants {
    public static String KAFKA_BROKER = "localhost:9092";
    public static String TOPIC_NAME = "tiki";
}
