package main.java.kafka;

import main.java.model.DataView;
import main.java.model_serializer.DataViewSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;

import java.util.Properties;

public class SimpleProducer {
    private Producer<Long, DataView> producer;

    public SimpleProducer(){
        this.producer =createProducer();
    }


    public static org.apache.kafka.clients.producer.Producer<Long, DataView> createProducer(){
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, IKafkaConstants.KAFKA_BROKER);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, DataViewSerializer.class);
        return new KafkaProducer<Long, DataView>(props);
    }

    public void send(DataView data){
        producer.send(new ProducerRecord<Long, DataView>("tiki_data_view", data));
    }

    public void close(){
        this.producer.close();
    }
}
