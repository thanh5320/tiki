package main.java.spark;

import com.google.gson.Gson;
import main.java.hbase.HbaseSink;
import main.java.kafka.SimpleProducer;
import main.java.model.DataEvent;
import main.java.model.DataView;
import main.java.model.DataView1;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.OutputMode;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class SparkJob {
    private SparkSession spark;
    private String urlDataEvent;
    private String urlDataView;

    public SparkJob(SparkSession spark, String urlDataEvent, String urlDataView){
        this.spark = spark;
        this.urlDataEvent = urlDataEvent;
        this.urlDataView = urlDataView;
    }

    public List<DataEvent> getListDataEvent(){
        Dataset<Row> data = spark.read().parquet(urlDataEvent);
        data= data.filter(data.col("cpa_value").contains("@@@100118"));
        List<Row> list = data.collectAsList();

        List<DataEvent> events = new ArrayList<>();
        for(int i=0;i< list.size();i++){
            long p0 = list.get(i).getLong(0);
            String p1 = list.get(i).getString(1);
            int p2 = list.get(i).getInt(2);
            String p3 = list.get(i).getString(3);
            String p4 = list.get(i).getString(4);
            String p5 = null;
            try {
                p5 = java.net.URLDecoder.decode(list.get(i).getString(5), StandardCharsets.UTF_8.name());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            ;
            String p6 = list.get(i).getString(6);
            String p7 = list.get(i).getString(7);
            long p8 = list.get(i).getLong(8);
            DataEvent dataSuccess = new DataEvent(p0,p5,p8);

            events.add(dataSuccess);
        }
        return events;
    }


    public List<DataView> getListDataView(){
        Dataset<Row> dataView = spark.read().parquet(urlDataView).select("time_group.time_create", "guid","domain", "path");
        List<Row> dataViewSelect = dataView.collectAsList();

        List<DataView> datas = new ArrayList<>();
        for(int i=0;i<dataViewSelect.size();i++){
            long p0 = dataViewSelect.get(i).getLong(0);
            long p1 = dataViewSelect.get(i).getLong(1);
            String p2 = dataViewSelect.get(i).getString(2);
            String p3 = dataViewSelect.get(i).getString(3);

            DataView data = new DataView(p0, p1, p2, p3);
            datas.add(data);
        }

        return datas;
    }

    public void pushDataViewToKafka(){
        List<DataView> datas = getListDataView();
        SimpleProducer producer = new SimpleProducer();
        for(DataView data: datas){
            producer.send(data);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        producer.close();
    }

    public void pustToHDFS(){
        for(int i=0;i<2;i++){
            String stri = Integer.toString(i);
            if(i<10){
                stri = "0"+stri;
            }
            for(int j=0;j<59;j=j+5) {
                String strj = Integer.toString(j);
                if (j < 10) {
                    strj = "0" + strj;
                }
                Dataset<Row> dataView = spark.read().parquet(urlDataView+"/parquet_logfile_at_"+stri+"h_"+strj+".snap").select("time_group.time_create", "guid");
                dataView.write().mode("append").save("hdfs://localhost:9000/task-tiki/");
                try {
                    Thread.sleep(1000*60);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void readDataViewFromKafka() throws TimeoutException, StreamingQueryException, IOException {

        Dataset<Row> dff = spark
                .readStream()
                .format("kafka")
                .option("kafka.bootstrap.servers", "localhost:9092")
                .option("subscribe", "tiki_data_view")
                .option("failOnDataLoss", false)
                .load()
                .selectExpr("CAST(value AS STRING)", "CAST(timestamp AS TIMESTAMP)");


        Encoder<DataView1> dataViewEncoder = Encoders.bean(DataView1.class);
        Dataset<DataView1> data = dff.map(new MapFunction<Row, DataView1>() {
            private static final long serialVersionUID = 1L;
            @Override
            public DataView1 call(Row value) throws Exception {
                Gson gson = new Gson();
                DataView dataViewObj = gson.fromJson(value.getString(0), DataView.class);
                DataView1 dataView = new DataView1(dataViewObj.getTimeCreate(), dataViewObj.getGuid(), dataViewObj.getDomain(), dataViewObj.getPath(),value.getTimestamp(1));
                return dataView;
            }
        }, dataViewEncoder);


        Dataset<Row> dataView = data.toDF();

        dataView.printSchema();

        dataView = dataView.filter(dataView.col("domain").contains("tiki")).filter(dataView.col("path").contains("checkout/payment/success"));

        Dataset<Row> dataEvent = spark.read().parquet("/home/thanhnv/task-tiki/data/2021_06_09");
        dataEvent= dataEvent.filter(dataEvent.col("cpa_value").contains("@@@100118"));

        dataView.createOrReplaceTempView("dataView");
        dataEvent.createOrReplaceTempView("dataEvent");


        spark.sql("select dataView.guid, dataView.timeStamp, dataEvent.cpa_value, dataEvent.time_create, dataView.timeCreate"
                + " from dataView inner join dataEvent on dataView.guid = dataEvent.guid").createOrReplaceTempView("dataJoin");


        Dataset<Row> dataResult = spark.sql("select guid, timeStamp, cpa_value, time_create, timeCreate from dataJoin where time_create between timeCreate and timeCreate - 3600000");

        dataResult=dataResult.withWatermark("timeStamp", "1 minutes");
        dataResult.createOrReplaceTempView("rs");

      spark.sql("select guid, timeStamp, max(time_create) as time from rs group by guid, timeStamp");



      StreamingQuery query = dataResult.writeStream()
              .foreach(new HbaseSink())
              .outputMode(OutputMode.Append())
              .start();

       query.awaitTermination();
    }
}
